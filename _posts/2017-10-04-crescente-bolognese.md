---
layout: post
title:  "Crescente bolognese"
date: 2017-10-04 14:00:00 +0200
categories: playtime
---
Piccolo intermezzo di svago, qualche giorno fa, grazie all'ultimo acquisto del team, [Paolo](http://paolino.codiceplastico.com), ed
alla sua fantastica *crescente* (a derivata rigorosamente positiva) *bolognese*, accompagnata da mortadella e brioche...

<a href="/images/crescente-bolognese/P_20170926_110713_web.jpg">
<img src="/images/crescente-bolognese/P_20170926_110713_web.jpg" width="400px" title="Salato..." alt="Salato..." />
</a>

<a href="/images/crescente-bolognese/P_20170926_110625_web.jpg">
<img src="/images/crescente-bolognese/P_20170926_110625_web.jpg" width="300px" title="... e dolce!" alt="... e dolce!" />
</a>